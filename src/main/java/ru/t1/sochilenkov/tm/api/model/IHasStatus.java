package ru.t1.sochilenkov.tm.api.model;

import ru.t1.sochilenkov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
