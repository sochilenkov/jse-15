package ru.t1.sochilenkov.tm.api.service;

import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project create(String name, String description);

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAll(Sort sort);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
