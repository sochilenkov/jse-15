package ru.t1.sochilenkov.tm.api.controller;

public interface IProjectController {

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void clearProjects();

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void showProjects();

    void startProjectById();

    void startProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
